package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn) {
        HashMap<String, Double> isbnPrices = new HashMap<>();

        isbnPrices.put("1", 10.0);
        isbnPrices.put("2", 45.0);
        isbnPrices.put("3", 20.0);
        isbnPrices.put("4", 35.0);
        isbnPrices.put("5", 50.0);

        if (isbnPrices.containsKey(isbn)) {
            return isbnPrices.get(isbn);
        } else {
            return 0;
        }
    }
}
